package ist.challenge.farus_fentahir.services;

import ist.challenge.farus_fentahir.models.User;
import ist.challenge.farus_fentahir.models.dto.UserDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface UserService {

    ResponseEntity<String> registUser(UserDto userDto);

    ResponseEntity<String> loginUser(UserDto userDto);

    List<User> getAllUsers();

    ResponseEntity<String> editUser(User user);

}
