package ist.challenge.farus_fentahir;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FarusFentahirApplication {

	public static void main(String[] args) {
		SpringApplication.run(FarusFentahirApplication.class, args);
	}

}
