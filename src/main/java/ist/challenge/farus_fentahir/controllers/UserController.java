package ist.challenge.farus_fentahir.controllers;

import ist.challenge.farus_fentahir.models.User;
import ist.challenge.farus_fentahir.models.dto.UserDto;
import ist.challenge.farus_fentahir.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {

    @Autowired
    private UserService userService;
    @PostMapping("/register")
    public ResponseEntity<String> register(UserDto userDto) {
        return userService.registUser(userDto);
    }
    @PostMapping("/login")
    public ResponseEntity<String> login(UserDto userDto) {
        return userService.loginUser(userDto);
    }
    @GetMapping("/")
    public List<User> getAllUsers(){
        return userService.getAllUsers();
    }

    @PutMapping("/edit/{id}")
    public ResponseEntity<String> edit(User user){
        return userService.editUser(user);
    }
}
