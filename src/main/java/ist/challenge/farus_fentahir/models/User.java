package ist.challenge.farus_fentahir.models;


import javax.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "username", length = 25, unique = true, nullable = false)
    private String username;

    @Column(name = "password", length = 25, nullable = false)
    private String password;

}
