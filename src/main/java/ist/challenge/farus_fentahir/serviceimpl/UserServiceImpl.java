package ist.challenge.farus_fentahir.serviceimpl;

import ist.challenge.farus_fentahir.models.User;
import ist.challenge.farus_fentahir.models.dto.UserDto;
import ist.challenge.farus_fentahir.repository.UserRepository;
import ist.challenge.farus_fentahir.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    public ResponseEntity<String> registUser(UserDto userDto) {
        Optional<User> existingUser = userRepository
                .findByUsername(userDto.getUsername());
        if (existingUser.isPresent()) {
            return ResponseEntity.status(HttpStatus.CONFLICT)
                    .body("Username already exist");
        }

        if (userDto.getUsername().length() >= 25 || userDto
                .getPassword().length() >= 25) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("Username or password must be no longer than 25 characters");
        }


        User newUser = new User();
        newUser.setUsername(userDto.getUsername());
        newUser.setPassword(userDto.getPassword());
        userRepository.save(newUser);

        return ResponseEntity.status(HttpStatus.CREATED)
                .body("User created Successfully");
    }

    public ResponseEntity<String> loginUser(UserDto userDto) {
        if (StringUtils.isEmpty(userDto.getUsername()) || StringUtils
                .isEmpty(userDto.getPassword())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("Username or password is required");
        }

        Optional<User> user = userRepository
                .findByUsername(userDto.getUsername());
        if (!user.isPresent() || !user.get().getPassword()
                .equals(userDto.getPassword())) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                    .body("Wrong username or password");
        }

        if (userDto.getUsername().length() >= 25 || userDto
                .getPassword().length() >= 25) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("Username or password must be no longer than 25 characters");
        }

        return ResponseEntity.ok("Login success");
    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public ResponseEntity<String> editUser(User user) {
        Optional<User> optUser = userRepository.findById(user.getId());
        if (!optUser.isPresent()){
            return ResponseEntity.notFound().build();
        }

        User anotherUser = optUser.get();

        if (!anotherUser.getUsername().equals(user.getUsername()) && userRepository
                .findByUsername(user.getUsername()).isPresent()){
            return ResponseEntity.status(HttpStatus.CONFLICT)
                    .body("Username already exist");
        }

        if (anotherUser.getPassword().equals(user.getPassword())){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("please choose another password or username");
        }

        anotherUser.setUsername(user.getUsername());
        anotherUser.setPassword(user.getPassword());
        userRepository.save(anotherUser);

        return ResponseEntity.ok("User updated");

    }

}
